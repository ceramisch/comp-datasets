GhOSt-PV Gold Standard of German Particle Verbs
Contains 400 German PVs with human ratings and associated information
Reference: ****
	
Authors: Stefan Bott, Nana Khvtisavrishvili, Sabine Schulte im Walde
Institute for Natural Language Processing
University of Stuttgart, Germany

Abbreviations: 
pv                  Particle Verb Lemma
bv                  Base Verb Lemma
prt                 Particle
pv_freq             Harmonic mean frequency of the PV in 4 corpora (HGC, SdeWaC, DeCow-12X, German Wikipedia)
bv_freq	            Frequency of the BV in the SDeWaC corpus
pv_frq_band         Frequency Band in Tertiles per Particle (Low, Mid, High)
ambig_lvl           Level of ambiguity of the PV (A1, A2, A3 and AG3 - 1, 2, 3 or more senses)
no_ratings          Number of ratings obtained / Number of raters that rated the element
mean_ratings        The mean value of the ratings
median_ratings      The median value of the ratings
variance            The variance of the rating value
stddev_ratings      The standard deviation of the ratings
prop_sep_occ        The proportion of syntactically separate occurrences of the PV
prop_non_sep_occ    The proportion of syntactically non-separate occurrences of the PV
