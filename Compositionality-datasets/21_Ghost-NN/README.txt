
This dataset contains the gold standard of German noun-noun compounds
described in Schulte im Walde et al. (LREC, 2016), GhoSt-NN. The gold
standard includes 868 compounds annotated with corpus frequencies of
the compounds and their constituents, productivity and ambiguity of
the constituents, semantic relations between the constituents, and
compositionality ratings of compound-constituent pairs. Moreover, a
subset of the compounds containing 180 compounds is balanced for the
productivity of the modifiers (distinguishing low/mid/high
productivity) and the ambiguity of the heads (distinguishing between
heads with 1, 2 and >2 senses)

For the computational models on compounds in Schulte im Walde et
al. (*SEM, 2016), we extended existing English resources to contain
similar information as GhoSt-NN. The extended information is also
provided.


----- DATA


The folder contains the following files:


- Ghost-NN_compound-candidates_freq.txt

  a set of 154,960 noun-noun candidate compounds and their
  constituents, accompanied by corpus frequency


- Ghost-NN_freq_prod_amb.txt

  the final gold standard Ghost-NN containing 868 noun-noun compounds,
  accompanied by corpus frequency, productivity, ambiguity


- Ghost-NN_relations.txt

  the final gold standard Ghost-NN containing 868 noun-noun compounds,
  annotated with semantic relations


- Ghost-NN_comp-means_expert.txt

  the final gold standard Ghost-NN containing 868 noun-noun compounds,
  annotated with compositionality means (over expert ratings);
  format: one line per compound


- Ghost-NN_comp-means_AMT.txt

  ditto, but with compositionality means over AMT worker ratings


- Ghost-NN_subset_20-9_balanced.txt

  a carefully balanced Ghost-NN subset of 20x9 compounds and their
  constituents, categorised according to our 9 criteria combinations
  for modifier productivity and head ambiguity


- Ghost-NN_subset_5-9_balanced.txt

  ditto, but containing only 5x9 compounds


The sub-folder EXT contains the following files:


- OS_freq_prod_amb.txt

  random selection of 396 compounds from O Seaghdha, accompanied by
  corpus frequency, productivity, ambiguity


- OS_comp-means.txt

  same random selection of 396 compounds from O Seaghdha, accompanied
  by expert compositionality ratings


- Reddy_freq_prod_amb.txt

  90 compounds from Reddy et al., accompanied by corpus frequency,
  productivity, ambiguity


- vdHB_freq_prod_amb.txt

  209 compounds from von der Heide & Borgwaldt, accompanied by corpus
  frequency, productivity, ambiguity



----- REFERENCES (see lrec-16_nn-gs.pdf and EXT/starsem-16_nn-gs-models.pdf):

Sabine Schulte im Walde, Anna Hätty, Stefan Bott, and Nana Khvtisavrishvili:
"GhoSt-NN: A Representative Gold Standard of German Noun-Noun Compounds"
In: Proceedings of the 10th International Conference on Language Resources and Evaluation.
Portoroz, Slovenia, 2016.

Sabine Schulte im Walde, Anna Hätty, Stefan Bott:
"The Role of Modifier and Head Properties in Predicting the Compositionality of English and German Noun-Noun Compounds: A Vector-Space Perspective"
In: Proceedings of the 5th Joint Conference on Lexical and Computational Semantics.
Berlin, Germany, August 2016.

