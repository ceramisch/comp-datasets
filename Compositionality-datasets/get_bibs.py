#!/usr/bin/env python3

import sys
import requests
import bibtexparser
from bibtexparser.bparser import BibTexParser
from bibtexparser.bwriter import BibTexWriter

import pdb

writer = BibTexWriter()
parser = BibTexParser()
parser.ignore_nonstandard_types = False
parser.interpolate_strings = False
#parser.homogenize_fields = False
#parser.common_strings = True

NonAnthology = ["https://ufal.mff.cuni.cz/pbml/112/art-savary-et-al.pdf", 
  "http://www.lrec-conf.org/proceedings/lrec2008/workshops/W20_Proceedings.pdf",
  "http://cs.unb.ca/~ccook1/CFS2008.pdf"]

ref = None
order = 1
with open("anthology-urls.txt") as AnthologyURLS :
  for url in AnthologyURLS :
    url = url.strip()
    biburl = (url[:-1] if url[-1] == "/" else url) + ".bib"    
    r = requests.get(biburl)
    ref = bibtexparser.loads(r.content, parser)    
    entry = ref.entries[-1]
    if 'abstract' in  entry.keys() :
      del(entry['abstract'])
    entry['order'] = "{:03}".format(order)
    order += 1
    print("\\citep{{{}}}".format(entry['ID']),end=" ")
    print("#" + biburl)
    
print("\n\n")

writer.order_entries_by = ("order")
print(bibtexparser.dumps(ref,writer))

print("\n\n")

for url in NonAnthology :
  print("# Could not obtain .bib for: \"{}\"".format(url))
