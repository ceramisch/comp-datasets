PACKAGE: German PNV triples in context (German_PNV_Fritzinger)

SUBMITTED BY: Fabienne Fritzinger, Marion Weller
              University of Stuttgart, Germany
              {fritzife,wellermn}@ims.uni-stuttgart.de

DATE: 04.07.2010

***************************************************
                 -- README file --				
***************************************************

The structure of the data collection is as follows:

<pnv>
 <type>preposition+noun+verb</type>
 <unit>
  <annotation>[idiomatic|literal|ambiguous|error]</annotation>
  <features>
    <determiner>[def|indef|dem|poss|quant|no]</determiner>
    <fused>[+|-]</fused>
    <number>[sg|pl|plsg]</number>
    <negated>[+|-]</negated>
    <vorfeld>[+|-]</vorfeld>
    <adjacency>[0-9]</adjacency>
  </features>
  <sentence>Contains the whole sentence</sentence>
 </unit>
 <unit>
  ....
 </unit>
</pnv>
<pnv>
...
</pnv>

--------------------------------------------------------------------------------

Description of annotation:

* possible values:  
      - idiomatic: expression has an idiomatic meaning in the present
        context
      - literal: expression has a literal meaning in the present context
      - ambiguous: the annotators could not determine whether the
        expression was used literally or idiomatically in the present
        context
      - error: indicates parsing or extraction errors

* for more details, see our LREC-2010 paper "A Survey of Idiomatic
  Preposition-Noun-Verb Triples on Token Level"

--------------------------------------------------------------------------------

Description of features:

* determiner: refers to the determiner of the noun

* fused: indicates whether the preposition is fused with a
  definite determiner: e.g. "im" = "in+dem"

* number: refers to the number of the noun

* negated: indicates whether the expression was negated or not. Please
  read our LREC-2010 Paper "Pattern-based Extraction of Negative
  Polarity Items from Dependency-parsed Text" if you wish more
  information on how we modelled negation.

* vorfeld: indicates whether the expression occurred at the beginning
  of the sentence (in the "vorfeld"). Idiomatic pnv's tend not to
  occur in this position.

* adjacency: indicates the distance of the pnv parts to one
  another. It is calculated using the positions of the words in the
  sentence (starting from 0): the positions of the preposition, the
  noun and the verb are summed up and divided by the pnv part in the
  middle of the other two. Adjacency scores close to "3" indicate that
  the pnv parts occur adjacently. Idiomatic pnvs tend to occur
  adjacently. Example:
 
  Wir lassen uns nicht unter Druck setzen .
  (we let us not under pressure put .)
   0       1        2      3      4         5         6     7
   adjacency = (4+5+6)/5 = 3.

--------------------------------------------------------------------------------

This folder also contains an alternative XML version of the candidates that is
compatible with the mwetoolkit (www.sourceforge.net/projects/mwetoolkit). The
awk script that converts between the two formats also cleans some internal 
annotation present in the original data. The conversion was performed as follows
  
# awk -f pnv2mwetoolkit.awk pnv_dataset_europarl.xml > pnv_dataset_europarl_mwetoolkit.xml
# awk -f pnv2mwetoolkit.awk pnv_dataset_faz.xml > pnv_dataset_faz_mwetoolkit.xml

We verify that the result is correct as follows

# xmllint --valid --stream --path dtd pnv_dataset_europarl_mwetoolkit.xml 
# xmllint --valid --stream --path dtd pnv_dataset_faz_mwetoolkit.xml 
    
This simple conversion script was implemented by Carlos Ramisch, questions can
be sent to carlosramisch@gmail.com
