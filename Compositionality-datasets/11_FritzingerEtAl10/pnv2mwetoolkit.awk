BEGIN{ 
	cand_id = 0; 
	print "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	print "<!DOCTYPE candidates SYSTEM \"dtd/mwetoolkit-candidates.dtd\">";
	print "<candidates>";
	print "<meta>";
	print "  <metafeat name=\"determiner\" type=\"{def,dem,no,poss,indef,quant}\"/>";
	print "  <metafeat name=\"number\" type=\"{sg,pl,sgpl}\"/>";	
	print "  <metafeat name=\"fused\" type=\"{+,-}\"/>";			
	print "  <metafeat name=\"negated\" type=\"{+,-}\"/>";		
	print "  <metafeat name=\"vorfeld\" type=\"{+,-}\"/>";	
	print "  <metafeat name=\"adjacendy\" type=\"real\"/>";		
	print "  <metatpclass name=\"annotation\" type=\"{idiomatic,ambiguous,error,literal}\"/>";		
	print "</meta>";
}
/<type>/{ 
	ngram=$0; 
	gsub(/<[^>]*>/, "",ngram); 
	gsub(/\r/, "",ngram); 	
	gsub(/ */, "",ngram); 		
	split(ngram,ngramlist,"+"); 
	ngram="  <ngram><w lemma=\"" ngramlist[1] "\" pos=\"P\" /> <w lemma=\"" ngramlist[2] "\" pos=\"N\" /> <w lemma=\"" ngramlist[3] "\" pos=\"V\" /> </ngram>"; 
}
/<annotation>/{
	tpclass = $1;
	gsub( /<[^>]*>/, "", tpclass); 
	gsub( /\r/, "", tpclass);
	gsub( /[\.-]/, "idiomatic", tpclass);	
	gsub( "#", "error", tpclass);		
	gsub( "=", "ambiguous", tpclass);			
	tpclass = "  <tpclass name=\"annotation\" value=\"" tpclass "\"/>";
}
/<determiner>/{
	featdet = $1;
	gsub(/<[^>]*>/, "", featdet); 
	gsub(/\r/, "", featdet); 		
	featdet = "    <feat name=\"determiner\" value=\"" featdet "\"/>";
}
/<fused>/{
	featfused = $1;
	gsub(/<[^>]*>/, "", featfused); 
	gsub(/\r/, "", featfused); 		
	featfused = "    <feat name=\"fused\" value=\"" featfused "\"/>";
}
/<number>/{
	featnumber = $1;
	gsub(/<[^>]*>/, "", featnumber); 
	gsub(/\r/, "", featnumber); 		
	featnumber = "    <feat name=\"number\" value=\"" featnumber "\"/>";
}
/<negated>/{
	featneg = $1;
	gsub(/<[^>]*>/, "", featneg); 
	gsub(/\r/, "", featneg); 		
	featneg = "    <feat name=\"negated\" value=\"" featneg "\"/>";
}
/<vorfeld>/{
	featvorfeld = $1;
	gsub(/<[^>]*>/, "", featvorfeld); 
	gsub(/\r/, "", featvorfeld); 		
	featvorfeld = "    <feat name=\"vorfeld\" value=\"" featvorfeld "\"/>";
}
/<adjacency>/{
	featadj = $1;
	gsub(/<[^>]*>/, "", featadj); 
	gsub(/\r/, "", featadj); 		
	featadj = "    <feat name=\"adjacency\" value=\"" featadj "\"/>";
}
/<sentence>/{
	sentence=$0; 
	gsub(/<[^>]*>/, "",sentence); 
	gsub(/\r/, "",sentence); 	
	gsub(/\"/, "\\&quot;",sentence); 		
	split(sentence,sentencelist," "); 
	sentence = "    <ngram> "
	lensentence = 0;
	for (wordindex in sentencelist) {
		lensentence++;
	}
	for (i=1; i<=lensentence;i++) {
		sentence = sentence "<w surface=\"" sentencelist[ i	 ] "\" />";
	}
	sentence = sentence "</ngram>"
}
/<\/unit>/{
	print "<cand candid=\"" cand_id++ "\">"; 
	print ngram; 
	print "  <occurs>";
	print sentence;
	print "  </occurs>";
	print "  <features>";
	print featdet;
	print featfused;	
	print featnumber;
	print featneg;
	print featvorfeld;
	print featadj;				
	print "  </features>";
	print tpclass;
	print "</cand>";
}
END{
	print "</candidates>";
}
