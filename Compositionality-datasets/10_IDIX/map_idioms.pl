use XML::Twig;
use Getopt::Long;
use File::Find;

my $idioms;
my $bnc;
my $out;
my %bncMap;
my $twig;
my $root;

parse_command_line();

# get all xml-files in the bnc directory and its sub-directories, populate a map with the file-paths
# as keys (this way, we'll only have to parse each xml file once)
find(
	sub { $bncMap{(split("/",$File::Find::name))[-1]} = $File::Find::name, if /\.xml$/ },
	$bnc
);

# open the idiom file, read in the lines and fill the map
open FILE, $idioms or die "Error: Cannot read idiom file!";
while (my $line = <FILE>) {
	# strip the line of leading and trailing whitespaces
	$line =~ s/\s*$//g;
	# this is the xml base-name
	$bncFile = (split("\t", $line))[1];
	# add the current line to the map, using the xml base-name as key
	$mapline = $bncMap{ $bncFile }.":".$line;
	# $bncMap{$bncFile} is a string looking like this: path/to/xml:idiom-line_1:idiom-line2[:...]
	$bncMap{ $bncFile } = $mapline;
}

foreach my $key (keys %bncMap) {
	$found = 0;
	@temp = split(":", $bncMap{$key});
	# this is the path to the actual xml file
	$file = $temp[0];
	# this contains instances of idioms
	@data = @temp[1...$#temp];

	# only process the xml file if it actualy contains instances of idioms
	if($#data > -1) {
		$found = 1;
		print "Parsing $file...\n";
		# parse the xml file
		$twig = new XML::Twig(twig_roots => {'wtext' => 1 }, keep_encoding => 1);
		$twig -> parsefile($file);
		print "Done. Now mapping idiom data...\n";

		# process the idioms, map integrate them into the bnc xml
		foreach my $chunk(@data){
			# current idiom and its instance
			@split = split("\t", $chunk);
			$idiom = (split("\"", $split[0]))[1];
			$sent = $split[2];
			$finding = $split[3];
			$label = $split[4];
			# an idiom might be split
			@words = @split[5...$#split];

			foreach my $wordspan(@words) {
				# get the sentence, starting and ending position of idiom
				$root = $twig -> root;
				$xml_sent = ($root -> descendants("s[\@n=\"$sent\"]"))[0];
				print "Mapping sentence $sent , words $wordspan\n";
				$start = (split("-", $wordspan))[0];
				$stop = (split("-", $wordspan))[1];

				# new <bncIdioms> element to be incorporated into the bnc xml
				$new = new XML::Twig(twig_roots => {'wtext' => 1 }, keep_encoding => 1);
				$new -> parse("<bncIdioms/>");
				$newRoot = $new -> root;
				$newRoot -> set_att(idiom => $idiom);
				$newRoot -> set_att(count => $finding);
				$newRoot -> set_att(label => $label);

				# cut the words between $start and $stop from the bnc xml, paste them as children
				# of the <bncIdioms> element
				if ($stop) {
					$w = ($xml_sent -> descendants("[\@n=\"$start\"]"))[0];
					$w_end = ($xml_sent -> descendants("[\@n=\"$stop\"]"))[0];

					# takes care of multi-word expressions
					if($w -> parent -> tag eq "mw") {
						$w = $w -> parent;
					}

					@siblings = $w -> next_siblings;

					$w -> cut;
					$w -> paste( last_child => $newRoot );

					# add all target words to the <bncIdioms>-element
					$end = 0;
					for $sibling(@siblings){
						# no multi-word expression
						if($sibling -> tag ne "mw"){
							# check if last target word has been reached
							if($sibling -> att("n") ne $w_end -> att("n")){
								$sibling -> cut;
								$sibling -> paste( last_child => $newRoot );
							}else{
								last;
							}
						# multi-word expression
						}else{
							# check if last target word is within mw expression
							foreach my $child($sibling -> descendants){
								if ($child -> att("n") eq $w_end -> att("n")){
									$end = 1;
									last;
								}
							}
							if($end == 0){
								$sibling -> cut;
								$sibling -> paste( last_child => $newRoot );
							}
						}
						if ($end == 1){
							last;
						}
					}

					if($w_end -> parent -> tag eq "mw") {
						$w_end = $w_end -> parent;
					}

					$w_end -> cut;
					$w_end -> paste ( last_child => $newRoot );
					#}
				} else {
					$w = ($xml_sent -> descendants("[\@n=\"$start\"]"))[0];
					$w -> cut;
					$w -> paste( last_child => $newRoot );
				}

				# cut <bncIdioms> element (now containing the words forming the idiom) from the new
				# xml, and paste it back into the bnc xml
				@try = $xml_sent -> descendants("[\@n=\"".($start-1)."\"]");
				$w;
				# the idiom might actually have been at the beginning of the sentence, this is taken
				# care of here
				if ($#try > -1) {
					$w = $try[0];
					$newRoot -> cut;
					$newRoot -> paste(after => $w);
				} else {
					$newRoot -> cut;
					$newRoot -> paste(first_child => $xml_sent);
				}
			}
		}
	}

	# if the bnc xml was changed, write it back to the specified output folder
	if ($found == 1) {
		print "writing to $out/$key...\n";
		open(FILE, ">$out/$key".".tmp") or die("Couldn't write new file");
		$twig -> print(\*FILE, 'indented');
		# workaround to not have the new xml file in a single line
		system("perl -p -e \"s/></>\\\\n</g\" $out/$key".".tmp > $out/$key");
		system("rm -f $out/$key".".tmp");
	}
}
print "All done!\n";

sub parse_command_line{
	GetOptions(
		"help" => sub {
			print "Usage:\tperl map_idioms.pl --switch=option\n\nSwitches:\t--idioms: path to idioms file";
			print "\n\t\t--bnc: folder containing bnc xml files\n\t\t--out: folder to write output to\n\t\t";
			print "--help: display this help\n";
			exit(0);
		},
		"idioms=s" => \$idioms,
		"bnc=s" => \$bnc,
		"out=s" => \$out
	);
}
