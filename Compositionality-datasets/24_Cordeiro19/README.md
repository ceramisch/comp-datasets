Compositionality of Nominal Compounds - Datasets
================================================

- Authors: Silvio Cordeiro, Carlos Ramisch, Aline Villavicencio, Leonardo Zilio, Marco Idiart, Rodrigo Wilkens
- Version 2.0 - August 16, 2022


Description
-----------

This package contains numerical judgements by native speakers on the
compositionality of 190 nominal compound in English (EN), 180 nominal
compounds in French (FR), and 180 nominal compounds in Brazilian
Portuguese (PT). The English data is split into two parts. The original
90 English compounds were annotated to complement the 90 compounds in
the Reddy dataset (see below). The \"extra\" 100 English compounds were
annotated to perform generalisation experiments in the [Computational
Linguistics](https://aclanthology.org/J19-1001/) paper (Section 6.3).

Judgements were obtained using Amazon Mechanical Turk (EN and FR) and a
web interface for volunteers (PT). Every compound has 3 scores:
compositionality of head word, compositionality of modifier word and
compositionality of the whole. Scores range from 1 (fully idiomatic) to
5 (fully compositonal) and are averaged over several annotators (around
10 to 20 depending on the language). All compounds also have synonyms
and similar expressions given by annotators.

The datasets are described in detail and used in the experiments of
papers below. Please cite one of them if you use this material in your
research.

-   [Unsupervised Compositionality Prediction of Nominal
    Compounds](https://aclanthology.org/J19-1001/)
    [\[bib\]](https://aclanthology.org/J19-1001/.bib)
-   [How Naked is the Naked Truth? A Multilingual Lexicon of Nominal
    Compound Compositionality](http://aclweb.org/anthology/P16-2026)
    [\[bib\]](http://aclweb.org/anthology/P16-2026.bib)
-   [Predicting the Compositionality of Nominal Compounds: Giving Word
    Embeddings a Hard Time](http://aclweb.org/anthology/P16-1187)
    [\[bib\]](http://aclweb.org/anthology/P16-1187.bib)
-   [Filtering and Measuring the Intrinsic Quality of Human
    Compositionality Judgments](http://aclweb.org/anthology/W16-1804)
    [\[bib\]](http://aclweb.org/anthology/W16-1804.bib)

Our methodology is inspired from [Reddy, McCarthy and Manandhar
(2011)](http://www.aclweb.org/anthology/I/I11/I11-1024.pdf). We include
their set of 90 compounds and judgments in our dataset for the analyses
and experiments on English in our papers above. However, we do not
include their dataset here, though. Please also [download their
data](http://www.dianamccarthy.co.uk/downloads.html) and cite their
paper to obtain a fully comparable English dataset to the one used in
our experiments.

Quick start
-----------

If you only want to use our datasets to evaluate your compositionality
prediction models, you\'re probably interested in the scores present in
the column named `compositionality` of files:

-   **annotations/unfiltered/en.unfiltered.csv**
-   **annotations/unfiltered/en-extra.unfiltered.csv**
-   **annotations/unfiltered/fr.unfiltered.csv**
-   **annotations/unfiltered/pt.unfiltered.csv**

Folders
-------

-   **annotations**: results, including raw files with individual
    annotations, averaged unfiltered and averaged filtered data, as well
    as quality metrics and distribution graphics (see filtering
    parameters below)
-   **bin**: scripts used to filter and estimate the quality of datasets
    (MWE workshop paper)
-   **compounds-lists**: contains the list of compounds and auxiliary
    information (gender, number, example sentences, etc) given to
    replace placeholders in MTurk questionnaire (in csv format for FR
    and EN) or used to create the dynamic HTML annotation interface (in
    MySQL database format for PT)
-   **questionnaires**: MTurk and HTML interfaces used in data
    collection. FR interface in HTML is included, but the data in this
    package comes from MTurk.

Files post-processing
---------------------

The following commands were executes in order to create most files in
the **annotations** folder.

```
# _Unfiltered averaged files used in ACL long and short paper_

for lg in en en-extra fr pt; do
  ../bin/filter-answers.py --zscore-thresh=10000000 --spearman-thresh=-1 \
    --batch-file raw/$lg.raw.csv --lang ${lg:0:2} \
    > unfiltered/$lg.unfiltered.csv \
    2> unfiltered/$lg.unfiltered.log
done

# _Generation of filtered averaged files used in MWE workshop paper_

for lg in en en-extra pt; do # fr has different thresholds
  ../bin/filter-answers.py --zscore-thresh=2.2 --spearman-thresh=0.5 \
    --batch-file raw/$lg.raw.csv --lang ${lg:0:2} \
    > filtered/$lg.filtered.csv 2> filtered/$lg.filtered.log
done    
../bin/filter-answers.py --zscore-thresh=2.5 --spearman-thresh=0.5 \
  --batch-file raw/fr.raw.csv --lang fr > filtered/fr.filtered.csv \
  2> filtered/fr.filtered.log

# _Generation of graphics and evaluation of datasets _
mkdir -p graphics
for lg in en en-extra fr pt; do
  for f in unfiltered filtered; do
    ../bin/intrinsic-quality-dataset.py --avg-file $f/$lg.$f.csv \
      2> quality/$lg.$f.quality
      mv $f/*.pdf graphics
  done
done
```
Note : Data may differ slightly from papers because we
added some new annotations since the papers were written. The 100
compounds in the EN-extra dataset were only used in the Computational
Linguistics paper but we provide filtering analyses here for comparison

