
	Brigitte Krenn's German PP-Verb Collocations (PNV)

This gold standard is a database of 21796 German combinations of prepositional phrase (PP) and governing verb, manually annotated by Brigitte Krenn [brigitte.krenn@researchstudio.at]. The PP-verb pairs have been pooled from a number of experiments [1,2,3,4,5], but all pairs were originally extracted from the Frankfurter Rundschau corpus [6].

PPs are represented by a combination of preposition and nominal head in the PP-verb pairs, so e.g. "auf der Bank", "auf einer Bank", "auf Baenken" and "auf die lange Bank" are all considered realisations of the same PP type "auf:Bank". Both the nominal head and the verb were lemmatised using the IMSLex morphology [7]. Because of this representation, the PP-verb combinations are also referred to as a PNV (preposition-noun-verb) database. Fused preposition-article combinations were normalised so that the definite article is indicated by a "+" character (e.g. both "im" = "in dem" and "ins" = "in das" are represented as "in+" in the database). Every combination of PP and lexical verb within the same clause (as annotated by the partial parser YAC [8]) was considered a PP-verb cooccurrence instance (in a rough approximation to the full identification of syntactic PP-verb relations); see [5, p. 39f] for details of the candidate extraction procedure.

All PP-verb pairs were manually annotated as lexical collocations or non-collocational by Brigitte Krenn [1]. In addition, a distinction was made between two subtypes of lexical collocations, support-verb constructions (FVG = "Funktionsverbgefuege") and figurative expressions (figur). Both FVG and figurative expressions are considered as lexical collocations, i.e. true positives (TPs). Further information on the annotation process can be found in [1], while [9] gives detailed annotation guidelines in German. Reliability of the annotation has been validated in [4], achieving kappa agreement scores above 75% for annotators with thorough linguistic training (and scores between 60% and 70% for non-specialised students in a computational linguistics degree).

The PNV database is provided as a TAB-delimited text table with 7 columns and a single header row specifying the names of the column variables:

	l1	PP (preposition : lemma of nominal head)
	l2	noun (lemma of lexical verb)
	b.FVG	pair is a support-verb construction (FVG)?
	b.figur	pair is a figurative expression?
	b.TP	pair is a true positive = lexical collocation?
	b.in.fr30	pair occurs at least 30 times in FR corpus?
	b.light.verb	verb is a typical "light verb" according to [10]?

Variables beginning with "b." contain Boolean values represented as "1" (True) and "0" (False) in the table. The file format is designed for the UCS software toolkit [11], which allows users to perform evaluation experiments very easily. It is also compatible with a wide range of other software, including spreadsheet programs (such as Excel or Numbers) and the open-source statistical programming environment R [12].

Since the German word pairs contain non-ASCII characters, two versions of the file are provided:

	de_pnv_krenn.latin1.adb  (in Latin1 = ISO-8859-1 encoding)
	de_pnv_krenn.utf8.adb    (in UTF-8 encoding)

The UCS toolkit is fully compatible only with the Latin1 encoding, while other software (in particular R) often works better with UTF-8 data.

The PP-verb pairs in the PNV database are an inhomogeneous collection of annotated data from various experiments. Researchers who prefer to work on a more systematic sample can restrict their evaluation to 5102 candidate pairs that occur at least 30 times in the Frankfurter Rundschau corpus. The variable "b.in.fr30" marks such frequent pairs, which are covered completely by the annotation database. The variable "b.light.verb" indicates pairs containing one of 16 typical "light verbs" suggested by [10]. Previous evaluation studies have shown that such "light verbs" can identify FVGs with relatively high accuracy [1] and that filtering considerably improves the performance of association measures [4, 147-150].

STATISTICAL INFORMATION

	items	percentage
total	21796	100.0%
TPs	1149	5.3%
FVG	549	2.5%
figur	600	2.8%
in.fr30	5102	23.4%
light.v	6892	31.6%

Baseline precision:
	TPs	FVG	figur
all	5.27%	2.52%	2.75%
in.fr30	11.09%	5.55%	5.55%
light.v	10.85%	6.64%	4.21%

COPYRIGHT AND CONTACT INFORMATION

Copyright (C) 2000-2005 by Brigitte Krenn and Stefan Evert.

Contact:
	Stefan Evert
	stefan.evert@uos.de
 	http://purl.org/stefan.evert/

	Brigitte Krenn
	brigitte.krenn@researchstudio.at
	http://www.ofai.at/~brigitte.krenn/

These data are made available under the terms of the Creative Commons Attribution-Noncommercial (CC-BY-NC) license, version 3.0 unported. You may use them for academic research and all non-commercial purposes as long as the authors (Brigitte Krenn, Stefan Evert) are properly credited. See

	http://creativecommons.org/licenses/by-nc/3.0/

for a full description and explanation of the licensing terms.

REFERENCES

[1] Krenn, Brigitte (2000). The Usual Suspects: Data-Oriented Models for the Identification and Representation of Lexical Collocations, volume 7 of Saarbruecken Dissertations in Computational Linguistics and Language Technology. DFKI & Universitaet des Saarlandes, Saarbruecken, Germany.

[2] Evert, Stefan and Krenn, Brigitte (2001). Methods for the qualitative evaluation of lexical association measures. In Proceedings of the 39th Annual Meeting of the Association for Computational Linguistics, pages 188-195, Toulouse, France.

[3] Evert, Stefan and Krenn, Brigitte (2005). Using small random samples for the manual evaluation of statistical association measures. Computer Speech and Language, 19(4), 450-466.

[4] Krenn, Brigitte; Evert, Stefan; Zinsmeister, Heike (2004). Determining intercoder agreement for a collocation identification task. In Proceedings of KONVENS 2004, pages 89-96, Vienna, Austria.

[5] Evert, Stefan (2004). The Statistics of Word Cooccurrences: Word Pairs and Collocations. Dissertation, Institut fuer maschinelle Sprachverarbeitung, University of Stuttgart. Published in 2005, URN urn:nbn:de:bsz:93-opus-23714. Available from http://www.collocations.de/phd.html.

[6] The FR corpus is part of the ECI Multilingual Corpus I distributed by ELSNET.  See http://www.elsnet.org/eci.html for more information and licensing conditions.

[7] Lezius, Wolfgang; Dipper, Stefanie; Fitschen, Arne (2000). IMSLex - representing morphological and syntactical information in a relational database. In U. Heid, S. Evert, E. Lehmann, and C. Rohrer (eds.), Proceedings of the 9th EURALEX International Congress, pages 133-139, Stuttgart, Germany.

[8] Kermes, Hannah (2003). Off-line (and On-line) Text Analysis for Computational Lexicography. Ph.D. thesis, IMS, University of Stuttgart. Arbeitspapiere des Instituts fuer Maschinelle Sprachverarbeitung (AIMS), volume 9, number 3.

[9] Krenn, Brigitte (2004). Manual zur Identifikation von Funktionsverbgefuegen und figurativen Ausdruecken in PP-Verb-Listen. http://www.collocations.de/guidelines/.

[10] Breidt, Elisabeth (1993). Extraction of N-V-collocations from text corpora: A feasibility study for German. In Proceedings of the 1st ACL Workshop on Very Large Corpora, Columbus, Ohio. (a revised version is available from http://arxiv.org/abs/cmp-lg/9603006).

[11] See http://www.collocations.de/software.html for more information and downloads.

[12] R Development Core Team (2003). R: A language and environment for statistical computing. R Foundation for Statistical Computing, Vienna, Austria. ISBN 3-900051-00-3. See also http://www.r-project.org/.

