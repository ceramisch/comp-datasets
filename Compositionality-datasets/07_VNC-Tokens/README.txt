PACKAGE: English VNC-Tokens (English_VNC_Cook)

SUBMITTED BY: Paul Cook
              University of Toronto (Canada)
              pcook@cs.toronto.edu

DATE: 01.02.2008

DESCRIPTION:  

This package contains 2984 potentially idiomatic English verb-noun
tokens, and their annotation as a literal, idiomatic, or unknown
usage.  The file VNC-Tokens contains the annotations and a reference
to the tokens, while documentation.pdf further describes the dataset.
