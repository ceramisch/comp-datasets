README
------
This is the README file for the Polish module of the Multilingual corpus of literal occurrences (LO) of multiword expressions. 

Corpus
-------
This corpus is a derivative work of the [PARSEME verbal multiword expressions (v 1.1)](http://hdl.handle.net/11372/LRT-2842) and is distributed under the same licenses.
For every verbal MWE (VMWE) annotated in the source corpus, candidate literal occurrences were automatically extracted, and then manually classified into 9 categories (including literal occurrences, coincidental occurrences, and several types of annotation errors) by native speakers. 

Provided annotations
--------------------
The data are organized into 9 columns:

* MWE - the lemmas of the lexicalized components of the source VMWE annotated in the PARSEME corpus

* POS-tags - the POS tag sequence of these lemmas

* category - the category of the source VMWE, according to the [PARSEME typology](http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.1/?page=030_Categories_of_VMWEs): IRV, LVC.full, LVC.cause, VID, VPC.full or VPC.semi.

* pre-annotation-methods - "human" if the candidate is a VMWE manually annotated in the source corpus; "WindowGap", "BagOfDeps", "UnlabeledDeps", or "LabeledDeps" if the candidate was extracted by one or more heuristics described in the reference publication (see below)

* sentence-with-mweoccur - source sentence with the LO candidate delimited by brackets

* source-sent-id - the identifier of the source sentence in the [PARSEME verbal multiword expressions (v 1.1)](http://hdl.handle.net/11372/LRT-2842)

* source-token-ranks - the ranks of the tokens in the original sentence which belong to the LO candidate

* annotation - one of the 9 categories selected by a human annotator (ERR-FALSE-IDIOMATIC, ERR-SKIPPED-IDIOMATIC, NONVERBAL-IDIOMATIC, MISSING-CONTEXT, WRONG-LEXEMES, COINCIDENTAL, LITERAL-MORPH, LITERAL-SYNT, or LITERAL-OTHER, see the reference publication below)

* comment - comment to the annotation (if any)

Author
----------
The annotation of literal, coincidental and erroneous occurrences of VMWEs (column 9) were performed by Agata Savary. For authorship of the other data see the original corpora.

License
----------
The POS-tag sequences (column 2) stem from the original corpora and are distributed under the terms of the [CC-BY-SA 0.4](https://creativecommons.org/licenses/by-sa/4.0/) license for the sentences with identifiers starting with 120-2-*, 310-* and 330-*, and under the terms of the ([GNU GPL v.3](https://www.gnu.org/licenses/gpl.html)) for other sentences.

All other data, including the annotation of literal, coincidental and erroneous occurrences of VMWEs (column 9), are distributed under the terms of the [CC-BY v4](https://creativecommons.org/licenses/by/4.0/) license.

Reference publication
----------
Agata Savary, Silvio Ricardo Cordeiro, Timm Lichte, Carlos Ramisch, Uxoa Iñurrieta and Voula Giouli (2019) "Literal occurrences of multiword expressions: Rare birds that cause a stir", in the Prague Bulletin of Mathematical Linguistics 112, Czech Republic. 

Contact
----------
agata.savary@univ-tours.fr

