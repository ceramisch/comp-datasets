# Compositionality prediction datasets

Compositionality prediction datasets gathered by Carlos Ramisch to perform a survey as part of his _Habilitation à Diriger des Recherches_.

Each folder in `compositionality-datasets` is prefixed with a number which identifies it in the (description table)[https://docs.google.com/spreadsheets/d/1wmlhJcPkqfadRp-rH_5lCEkwLRgDCrCF2OYE3iP_qUg/edit?usp=sharing] (if Google Docs is not available, a dump is present in this repository, in `Compositionality_datasets_SOTA_dump20230829.ods`

A discussion of these datasets in the form of a survey can be found in `comp-datasets-survey-20230829.pdf` (excerpt from the Habilitation manuscript).

## License

The status of each dataset's licence should be checked before using it. Please, email the author of this repository first.last at lis-lab.fr if you find any issues or if you are the owner of one of these datasets and would like to see your data removed from this repository.
